var Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
};

Bicicleta.prototype.toString = function(){
    return 'id' + this.id + ' | Color: ' + this.color;
}

Bicicleta.allBicis = [];

Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
};

Bicicleta.findById = function(aBiciId){

    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    
    if(aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBici}`);
};

Bicicleta.removeById = function(aBiciId){

    Bicicleta.allBicis = Bicicleta.allBicis.filter(function(value, index, array){

        return value.id != aBiciId;
    });
};

var a = new Bicicleta(1, 'rojo', 'urbana', [39.469164, -0.377159]);
var b = new Bicicleta(2, 'blanco', 'urbana', [39.467216, -0.374961]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;